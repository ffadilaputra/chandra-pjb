<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_masuk extends CI_Controller {

	public function index()
	{
		
		$this->load->model('tools_masuk_model');
		$data["tools_masuk_list"] = $this->tools_masuk_model->getDataToolsMasuk();
		$this->load->view('supervisor/tools/v_tools_masuk', $data);	
	}

	public function datatable()
	{
		$this->load->model('Tools_masuk_model');
		$data["tools_masuk_list"] = $this->tools_masuk_model->getDataToolsMasuk();
		$this->load->view('supervisor/tools/v_tools_masuk', $data);			
	}
		public function create()
	{

		$this->load->model('Tools_Masuk_Model');
		
		$data["tools_masuk_list"] = $this->Tools_Masuk_Model->getDataToolsMasuk();
		$data["tools_list"] = $this->Tools_Masuk_Model->getTools();

		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('tanggal_masuk', 'Tanggal_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		// $this->form_validation->set_rules('nama_sparepart_masuk', 'nama_sparepart_masuk', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('supervisor/tools/tambah_tools_masuk', $data);
		}
				else{
					$this->Tools_Masuk_Model->insertToolsMasuk();
					// $this->Sparepart_Masuk_Model->statusdipinjam();
					$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
					redirect('supervisor/tools_masuk','refresh');
				}
			}
		public function updatestatus($kode_masuk)
	{
		//load libraryl
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_masuk', 'Kode_masuk', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		// $this->load->model('Sparepart_Masuk_Model');
		// $data['sparepart_masuk']=$this->Sparepart_Masuk_Model->getSparepartMasuk($kode_masuk);
		
		// if($this->form_validation->run()==FALSE){

		// //setelah load data dikirim ke view
		// 	redirect('sparepart_masuk/index');

		// }else{
			$this->load->model('Tools_Masuk_Model');
			$this->Tools_Masuk_Model->UpdateByStatus($kode_masuk);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('supervisor/tools_masuk/index');
// }
		
	}
}