<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman_Sparepart extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function datatable()
	{
		$this->load->model('Peminjaman_Sparepart_Model');
		$data["lala_list"] = $this->Peminjaman_Sparepart_Model->getDataPeminjaman();
		$data["sparepart_masuk_list"] = $this->Peminjaman_Sparepart_Model->getMasuk();
		$data["user_list"] = $this->Peminjaman_Sparepart_Model->getUser();
		$this->load->view('tambah_peminjaman_sparepart_view',$data);	
	}

	public function index()
	{
		$this->load->model('Peminjaman_Sparepart_Model');
		$data["lala_list"] = $this->Peminjaman_Sparepart_Model->getDataPeminjaman();
		$data["sparepart_masuk_list"] = $this->Peminjaman_Sparepart_Model->getMasuk();
		$data["user_list"] = $this->Peminjaman_Sparepart_Model->getUser();
		//var_dump($data["lala_list"]);
		$this->load->view('tambah_peminjaman_sparepart_view',$data);

	}	

	public function create()
	{

		$this->load->model('Peminjaman_Sparepart_Model');
		
		$data["user_list"] = $this->Peminjaman_Sparepart_Model->getUser();
		$data["sparepart_list"] = $this->Peminjaman_Sparepart_Model->getDataPeminjaman();
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_peminjaman', 'Kode_peminjaman', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('id_user', 'id_user', 'trim|required');
		// $this->form_validation->set_rules('tanggal_peminjaman', 'Tanggal_peminjaman', 'trim|required');
		// $this->form_validation->set_rules('batas_pengembalian', 'Tanggal_pengembalian', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('tambah_peminjaman_sparepart_view', $data);
		}
			else
			{
					$this->Peminjaman_Sparepart_Model->insertPeminjaman();
					$data["sparepart_list"] = $this->Peminjaman_Sparepart_Model->getDataPeminjaman();
					$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
					redirect('peminjaman_sparepart','refresh');
				
			}
	}

	public function update($kode_peminjaman)
	{
		// //load libraryl
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('tgl_peminjaman', 'Tgl_peminjaman', 'trim|required');
		$this->form_validation->set_rules('nama_sparepart', 'nama_sparepart', 'trim|required');
		$this->form_validation->set_rules('tanggal_dikembalikan', 'Tanggal_dikembalikan', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');



		
		//sebelum update data harus ambil data lama yang akan di update
		$this->load->model('Peminjaman_Sparepart_Model');
		$data['peminjaman_sparepart']=$this->Peminjaman_Sparepart_Model->getDataPeminjamanById($kode_peminjaman);

			$this->Peminjaman_Sparepart_Model->updateById($kode_peminjaman);
		//sebelum update data harus ambil data lama yang akan di update
		// $this->load->model('Peminjaman_Sparepart_Model');
		// $data['peminjaman']=$this->Peminjaman_Sparepart_Model->getDataPeminjamanById($kode_peminjaman);
		// $data["masuk_list"] = $this->Peminjaman_Sparepart_Model->getMasuk();
		// $data["sparepart_list"] = $this->Peminjaman_Sparepart_Model->getSparepart();
		// $data["user_list"] = $this->Peminjaman_Sparepart_Model->getUser();	
		// skeleton code
		if($this->form_validation->run()==FALSE){

		// setelah load data dikirim ke view
			$this->load->view('kembalikan_sparepart',$data);

		}else{
			$this->Peminjaman_Sparepart_Model->updateById($kode_peminjaman);
			$this->load->view('peminjaman_sparepart');

	}


		
	}

	public function delete($kode_peminjaman)
 	{ 
 	 	$this->load->model('Peminjaman_Sparepart_Model');
  		$this->Peminjaman_Sparepart_Model->deleteByKode($kode_peminjaman);
 	 	redirect('peminjaman_sparepart');
	 }

	 public function TransaksiPeminjaman()
	{
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kode_peminjaman', 'Kode_peminjaman', 'trim|required');
		$this->load->model('Peminjaman_Sparepart_Model');
		$data["keranjang_list"] = $this->Peminjaman_Sparepart_Model->getKeranjang();
		if($this->form_validation->run()==FALSE){

				$page='peminjaman_sparepart';		
	    }else{
	    	 $keranjang=$this->Peminjaman_Sparepart_Model->tampilKeranjang()->result();
        foreach($keranjang as $row){
            $info=array(
                'peminjaman_id'=>$this->input->post('kode_peminjaman'),
                'user_id'=>$row->id_user,
                'sparepart_id'=>$row->kode_sparepart,
                'jumlah'=>1,
                'jatuh_tempo'=>$this->input->post('jatuh_tempo'),
                'status'=>0
            );
            $this->PeminjamanModel->simpan($info);

        }
	    	$this->PeminjamanModel->insertTransaksi();
	    	$this->PeminjamanModel->truncateKeranjang();
	    	
	    	// $this->PeminjamanModel->truncateKeranjang();
			$data["keranjang_list"] = $this->Peminjaman_Sparepart_Model->getKeranjang();	
			$page='peminjaman_sparepart';
		}

	}

	public function pengembaliaSparepart($id){
		$this->load->model('Peminjaman_Sparepart_Model');
		$data['peminjaman_sparepart']=$this->Peminjaman_Sparepart_Model->getDataPeminjamanById($id);
		$this->Peminjaman_Sparepart_Model->pengembalianBarang($id);
		redirect('Peminjaman_Sparepart/create');	
	}

	

}


/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */
