<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_user extends CI_Controller {

	public function index()
	{
		$this->load->model('User');
		$data["user_list"] = $this->User->getDataUser();
		$this->load->view('data_user', $data);		
	}

	public function datatable()
	{
		$this->load->model('User');
		$data["user_list"] = $this->User->getDataUser();
		$this->load->view('data_user', $data);	
	}
	// 	public function create()
	// {

	// 	$this->load->model('Sparepart_Masuk_Model');
		
	// 	$data["sparepart_masuk_list"] = $this->Sparepart_Masuk_Model->getDataSparepartMasuk();
	// 	$data["sparepart_list"] = $this->Sparepart_Masuk_Model->getSparepart();

	// 	$this->load->helper('url','form');	
	// 	$this->load->library('form_validation');
	// 	// $this->form_validation->set_rules('tanggal_masuk', 'Tanggal_masuk', 'trim|required');
	// 	$this->form_validation->set_rules('jumlah_masuk', 'Jumlah_masuk', 'trim|required');
	// 	// $this->form_validation->set_rules('nama_sparepart_masuk', 'nama_sparepart_masuk', 'trim|required');
	// 	$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');

	// 	if($this->form_validation->run()==FALSE){
	// 		$this->load->view('sinfo/sparepart/tambah_sparepart_masuk', $data);
	// 	}
	// 			else{
	// 				$this->Sparepart_Masuk_Model->insertSparepartMasuk();
	// 				// $this->Sparepart_Masuk_Model->statusdipinjam();
	// 				$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
	// 				redirect('sinfo/sparepart_masuk','refresh');
	// 			}
	// 		}
}