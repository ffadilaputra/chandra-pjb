<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 
 class Register extends CI_Controller {
     
     function __construct(){
         parent::__construct();
         $this->load->library(array('form_validation'));
         $this->load->helper(array('url','form'));
         $this->load->model('User'); //call model
     }
 
     public function index() {
 
         $this->form_validation->set_rules('nama', 'NAMA','required');
          $this->form_validation->set_rules('bagian', 'BAGIAN','required');
         $this->form_validation->set_rules('username', 'USERNAME','required|is_unique[user.username]');
         $this->form_validation->set_rules('password','PASSWORD','required');
         $this->form_validation->set_rules('status','Status','required');
         if($this->form_validation->run() == FALSE) {
             $this->load->view('register');
         }else{
 
             $data['nama']   =    $this->input->post('nama');
              $data['bagian']   =    $this->input->post('bagian');
             $data['username'] =    $this->input->post('username');
             $data['password'] =    md5($this->input->post('password'));
             $data['status'] =    $this->input->post('status');
 
             $this->User->daftar($data);
             
             $pesan['message'] =    "Berhasil Mendaftar";
             
             $this->load->view('home_admin',$pesan);
         }
     }
 }