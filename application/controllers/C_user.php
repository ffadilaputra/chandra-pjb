<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
	}
	public function index() {
		$data['username'] = $this->session->userdata('username');
		$this->load->view('home_user', $data);
	}

	public function logout() {
		$this->session->unset_userdata('username');
		// $this->session->unset_userdata('status');
		session_destroy();
		redirect('auth');
	}
}

/* End of file C_user.php */
/* Location: ./application/controllers/C_user.php */