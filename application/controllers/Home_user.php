<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_user extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('home_user');			
	}

}

/* End of file Home_user.php */
/* Location: ./application/controllers/Home_user.php */