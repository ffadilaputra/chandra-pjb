<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart extends CI_Controller {

	public function index()
	{
		$this->load->model('Sparepart_Model');
		$data["sparepart_list"] = $this->Sparepart_Model->getDataSparepart();
		$this->load->view('sparepart',$data);		
	}

		public function datatable()
	{
		$this->load->model('Sparepart_Model');
		$data["sparepart_list"] = $this->Sparepart_Model->getDataSparepart();
		$this->load->view('sparepart',$data);	
	}

	public function create()
	{
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_sparepart', 'Nama_sparepart', 'trim|required');	
		// $this->form_validation->set_rules('jumlah_sparepart', 'jumlah_sparepart', 'trim|required');	
		$this->form_validation->set_rules('deskripsi_sparepart', 'deskripsi_sparepart', 'trim|required');	
		$this->load->model('Sparepart_Model');	
		if($this->form_validation->run()==FALSE){
			$this->load->view('tambah_sparepart_view');
		}
			else
			{
				$this->Sparepart_Model->insertSparepart();
				$this->session->set_flashdata('pesan','tambah data berhasil');
				redirect('sparepart/index');

			}
	}

	public function update($kode_sparepart)
	{
		//load library
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_sparepart', 'Nama_sparepart', 'trim|required');	
		// $this->form_validation->set_rules('jumlah_sparepart', 'jumlah_sparepart', 'trim|required');	
		$this->form_validation->set_rules('deskripsi_sparepart', 'deskripsi_sparepart', 'trim|required');	
		
		//sebelum update data harus ambil data lama yang akan di update
		$this->load->model('Sparepart_Model');
		$data['sparepart']=$this->Sparepart_Model->getSparepart($kode_sparepart);
		//skeleton code
		if($this->form_validation->run()==FALSE){

		//setelah load data dikirim ke view
			$this->load->view('edit_sparepart_view',$data);

		}else{
			$this->Sparepart_Model->updateByKode($kode_sparepart);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('sparepart/index');

		}
	}
	public function delete($kode_sparepart)
	{
		$this->load->model('Sparepart_Model');
		$this->Sparepart_Model->deleteByKode($kode_sparepart);
		redirect('sparepart');
	}

}

