<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_sparepart_masuk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('sparepart_masuk_model');
	}

	
	public function index()
	{
		$this->load->model('Sparepart_Masuk_Model');
		$data["sparepart_masuk_list"] = $this->Sparepart_Masuk_Model->getDataSparepartMasuk();
		$this->load->view('laporan_sparepart_masuk',$data);
 
 header("Content-type: application/vnd-ms-word");
 
 header("Content-Disposition: attachment; filename=Laporan_sparepart_masuk.doc");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
	}

	// public function export(){
	// 	$this->load->model('sparepart_masuk_model');
	// $data["sparepart_masuk_list"] = $this->sparepart_masuk_model->getDataSparepartMasuk();
	// 	$this->load->view('laporan_sparepart_masuk', $data);

 	


}
?>