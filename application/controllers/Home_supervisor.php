<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_supervisor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('home_supervisor');	
	}

}

/* End of file Home_admin.php */
/* Location: ./application/controllers/Home_admin.php */