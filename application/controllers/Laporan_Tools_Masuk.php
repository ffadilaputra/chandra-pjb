<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_Tools_Masuk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('tools_masuk_model');
	}

	
	public function index()
	{
		$this->load->model('Tools_Masuk_Model');
		$data["tools_masuk_list"] = $this->Tools_Masuk_Model->getDataToolsMasuk();
		$this->load->view('laporan_tools_masuk',$data);
 
 header("Content-type: application/vnd-ms-word");
 
 header("Content-Disposition: attachment; filename=laporan_tools_masuk.doc");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
	}

	// public function export(){
	// 	$this->load->model('sparepart_masuk_model');
	// $data["sparepart_masuk_list"] = $this->sparepart_masuk_model->getDataSparepartMasuk();
	// 	$this->load->view('laporan_sparepart_masuk', $data);

 	


}
?>