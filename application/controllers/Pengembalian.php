<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function index()
	{
	$this->load->model('Pengembalian_model');
	$data["pengembalian_list"] = $this->Pengembalian_model->getDataPengembalian();
	$this->load->view('pengembalian',$data);

	}	

	public function datatable()
	{
		$this->load->model('pengembalian_model');
		$data["pengembalian_list"] = $this->pengembalian_model->getDataPengembalian();
		$this->load->view('pengembalian',$data);	
	}

	public function insertPengembalian()
	{

		$this->load->model('Pengembalian_Model');

		$this->load->helper('url','form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id_peminjaman', 'Id_peminjaman', 'trim|required');
		$this->form_validation->set_rules('id_user', 'Id_peminjam', 'trim|required');
		$this->form_validation->set_rules('id_buku', 'Id_buku_dipinjam', 'trim|required');
		$this->form_validation->set_rules('tanggal_dikembalikan', 'tanggal_kembali', 'trim|required');
		$this->form_validation->set_rules('jatuhtempo', 'jatuhtempo', 'trim|required');
		$this->form_validation->set_rules('denda', 'denda', 'trim|required');

		$this->load->model('pengembalian_model');
		$this->pengembalian_model->insertPengembalian();
		$this->pengembalian_model->updatestatusbuku();
		$this->pengembalian_model->updatestatuspeminjaman();
		redirect('pengembalian','refresh');
			}
	}

// 	public function update($id_peminjaman)
// 	{
// 		//load libraryl
// 		$this->load->helper('url','form');	
// 		$this->load->library('form_validation');
// 		$this->form_validation->set_rules('id_peminjaman', 'Id_peminjaman', 'trim|required');
// 		$this->form_validation->set_rules('id_user', 'Id_peminjam', 'trim|required');
// 		$this->form_validation->set_rules('id_buku', 'Id_buku_dipinjam', 'trim|required');
// 		$this->form_validation->set_rules('tanggal_dikembalikan', 'tanggal_kembali', 'trim|required');
// 		$this->form_validation->set_rules('jatuhtempo', 'jatuhtempo', 'trim|required');
// 		$this->form_validation->set_rules('denda', 'denda', 'trim|required');



		
// 		//sebelum update data harus ambil data lama yang akan di update
// 		$this->load->model('peminjaman_model');
// 		$data['peminjaman']=$this->peminjaman_model->getDataPeminjamanById($id_peminjaman);
// 		// $data["kategori_list"] = $this->peminjaman_model->getKategori();
// 		// $data["penerbit_list"] = $this->peminjaman_model->getPenerbit();
// 		// $data["pengarang_list"] = $this->peminjaman_model->getPengarang();	
// 		//skeleton code
// 		if($this->form_validation->run()==FALSE){

// 		//setelah load data dikirim ke view
// 			$this->load->view('peminjaman',$data);

// 		}else{
// 			$this->peengembalian_model->insertPengembalian($id_peminjaman);
// 			$this->load->view('edit_peminjaman_sukses');

// 		}
// 	}

// 	public function kembalikan($id_peminjaman)
// 	{
// 		//load libraryl
// 		$this->load->helper('url','form');	
// 		$this->load->library('form_validation');
// 		$this->form_validation->set_rules('id_peminjaman', 'IdPinjam', 'trim|required');
// 		$this->form_validation->set_rules('id_user', 'Nim', 'trim|required');
// 		$this->form_validation->set_rules('id_buku', 'Buku', 'trim|required');
// 		$this->form_validation->set_rules('jatuhtempo', 'Tempo', 'trim|required');
// 		$this->form_validation->set_rules('denda', 'Denda', 'trim|required');
// 		$this->form_validation->set_rules('tanggal_kembali', 'TglKembali', 'trim|required');



		
// 		//sebelum update data harus ambil data lama yang akan di update
// 		// $this->load->model('peminjaman_model');
// 		// $data['peminjaman']=$this->peminjaman_model->updatekembali($id_peminjaman);
// 		// $data["kategori_list"] = $this->peminjaman_model->getKategori();
// 		// $data["penerbit_list"] = $this->peminjaman_model->getPenerbit();
// 		// $data["pengarang_list"] = $this->peminjaman_model->getPengarang();	
// 		// //skeleton code
// 		// if($this->form_validation->run()==FALSE){

// 		// //setelah load data dikirim ke view
// 		// 	$this->load->view('pengembalian',$data);

// 		// }else{
// 		// 	$this->peminjaman_model->updateById($id_peminjaman);
// 		// 	$this->load->view('edit_peminjaman_sukses');
// 		// }
// 	}

// 	public function delete($id_peminjaman)
//  	{ 
//  	 	$this->load->model('peminjaman_model');
//   		$this->peminjaman_model->deleteById($id_peminjaman);
//  	 	redirect('peminjaman');
// 	 }

	


/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */
