<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart extends CI_Controller {

	public function index()
	{
		$this->load->model('sparepart_masuk_model');
		$data["sparepart_list"] = $this->sparepart_masuk_model->getDataSparepartMasuk();
		$this->load->view('user/sparepart/v_sparepart_masuk', $data);		
	}

	public function datatable()
	{
		$this->load->model('sparepart_masuk_model');
		$data["sparepart_list"] = $this->sparepart_masuk_model->getDataSparepartMasuk();
		$this->load->view('user/sparepart/v_sparepart_masuk', $data);	
	}
}