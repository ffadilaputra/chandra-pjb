<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller {

	public function index()
	{
		$this->load->model('tools_masuk_model');
		$data["tools_list"] = $this->tools_masuk_model->getDataToolsMasuk();
		$this->load->view('user/tools/v_tools', $data);		
	}

	public function datatable()
	{
		$this->load->model('tools_masuk_model');
		$data["tools_list"] = $this->tools_masuk_model->getDataToolsMasuk();
		$this->load->view('user/tools/v_tools', $data);	
	}
}