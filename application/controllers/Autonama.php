<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Autonama extends CI_Controller
{
 
    public function __construct() {
        parent::__construct();
        $this->load->model('Peminjaman_Model'); //load model mkota yang berada di folder model
        $this->load->helper(array('url')); //load helper url
 
    }
 
 
    public function get_allnama() {
        $kode = $this->input->post('kode',TRUE); //variabel kunci yang di bawa dari input text id kode
        $query = $this->Peminjaman_Model->get_allnama(); //query model
 
        $nim       =  array();
        foreach ($query as $d) {
            $nim[]     = array(
                'label' => $d->nim, //variabel array yg dibawa ke label ketikan kunci
                'nim' => $d->nim, 
                'nama' => $d->nama //variabel yg dibawa ke id nama
                
            );
        }
        echo json_encode($nim);      //data array yang telah kota deklarasikan dibawa menggunakan json
    }
}