<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_Masuk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	
	public function index()
	{
		
		$this->load->model('Tools_Masuk_Model');
		$data["tools_masuk_list"] = $this->Tools_Masuk_Model->getDataToolsMasuk();
		$data["tools_list"] = $this->Tools_Masuk_Model->getTools();
		$this->load->view('tools_masuk', $data);


	}	

	public function datatable()
	{
		$this->load->model('Tools_Masuk_Model');
		$data["tools_masuk_list"] = $this->Tools_Masuk_Model->getDataToolsMasuk();
		$this->load->view('tools_masuk',$data);	
	}


	public function create()
	{

		$this->load->model('Tools_Masuk_Model');
		
		$data["tools_masuk_list"] = $this->Tools_Masuk_Model->getDataToolsMasuk();
		$data["tools_list"] = $this->Tools_Masuk_Model->getTools();

		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('tanggal_masuk', 'Tanggal_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		// $this->form_validation->set_rules('nama_tools_masuk', 'nama_tools_masuk', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('tambah_tools_masuk_view', $data);
		}
				else{
					$this->Tools_Masuk_Model->insertToolsMasuk();
					// $this->tools_Masuk_Model->statusdipinjam();
					$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
					redirect('tools_masuk','refresh');
				}
			}

	public function update($kode_masuk)
	{
		//load libraryl
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_masuk', 'Kode_masuk', 'trim|required');
		$this->form_validation->set_rules('tanggal_masuk', 'Tanggal_masuk', 'trim|required');
		// $this->form_validation->set_rules('nama_tools_masuk', 'Nama_tools_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');




		
		$this->load->model('Tools_Masuk_Model');
		$data['tools_masuk']=$this->Tools_Masuk_Model->getToolsMasuk($kode_masuk);
		
		if($this->form_validation->run()==FALSE){

		//setelah load data dikirim ke view
			$this->load->view('edit_tools_masuk_view',$data);

		}else{
			$this->Tools_Masuk_Model->UpdateByKode($kode_masuk);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('tools_masuk/index');

		}
	}
	

	public function delete($kode_masuk)
 	{ 
 	 	$this->load->model('tools_Masuk_Model');
  		$this->tools_Masuk_Model->deleteByKode($kode_masuk);
 	 	redirect('tools_masuk');
	 }

	public function updatestatus($kode_masuk)
	{
		//load libraryl
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_masuk', 'Kode_masuk', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		// $this->load->model('tools_Masuk_Model');
		// $data['tools_masuk']=$this->tools_Masuk_Model->gettoolsMasuk($kode_masuk);
		
		// if($this->form_validation->run()==FALSE){

		// //setelah load data dikirim ke view
		// 	redirect('tools_masuk/index');

		// }else{
			$this->load->model('tools_Masuk_Model');
			$this->tools_Masuk_Model->UpdateByStatus($kode_masuk);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('tools_masuk/index');
// }
		
	}



   
}

/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */