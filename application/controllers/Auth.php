<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index() {
		$this->load->view('login_view');
		$this->load->library('session');
	}

	public function cek_login() {
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => md5($this->input->post('password', TRUE))
			);
		$this->load->model('Model_user'); // load model_user
		$hasil = $this->Model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['id_user'] = $sess->id_user;	
				$sess_data['username'] = $sess->username;
				$sess_data['status'] = $sess->status;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('status')=='admin') {
				redirect('c_admin');
			}
			elseif ($this->session->userdata('status')=='user') {
				redirect('c_user');
			}		
			elseif ($this->session->userdata('status')=='sinfo') {
				redirect('C_sinfo');
			}
			elseif ($this->session->userdata('status')=='supervisor') {
				redirect('C_supervisor');
			}
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}

}

?>