<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart_Keluar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function datatable()
	{
			$this->load->model('Sparepart_Keluar_Model');
		$data["sparepart_keluar_list"] = $this->Sparepart_Keluar_Model->getDataSparepartKeluar();
		$this->load->view('sparepart_keluar',$data);	
	}
	public function index()
	{
		/*$this->load->model('buku_model');
		$data["buku_list"] = $this->buku_model->getDataBuku();
		$this->load->view('buku',$data);*/
		$this->load->model('Sparepart_Keluar_Model');
		$data["sparepart_keluar_list"] = $this->Sparepart_Keluar_Model->getDataSparepartKeluar();
		$data["sparepart_list"] = $this->Sparepart_Keluar_Model->getSparepart();
		
		// $data["pengarang_list"] = $this->Sparepart_keluar_Model->getPengarang();	
		$this->load->view('sparepart_keluar', $data);
	}	

	public function create()
	{

		$this->load->model('Sparepart_Keluar_Model');
		
		$data["sparepart_keluar_list"] = $this->Sparepart_Keluar_Model->getDataSparepartKeluar();
		$data["sparepart_list"] = $this->Sparepart_Keluar_Model->getSparepart();

		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('tanggal_keluar', 'Tanggal_keluar', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		// $this->form_validation->set_rules('nama_sparepart_keluar', 'nama_sparepart_keluar', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('tambah_sparepart_keluar_view', $data);
		}
			
				// $this->load->model('Sparepart_keluar_Model');

				
				// $this->load->view('tambah_peminjaman_sukses');

				// $overload = $this->Sparepart_keluar_Model->Max();
				// if ($overload>=5) {
				// 	$this->session->set_flashdata('overload', 'sudah mencapai batas peminjaman 5 peminjaman');
				// 	$this->load->view('tambah_peminjaman_view', $data);
				
				else{
					$this->Sparepart_Keluar_Model->insertSparepartKeluar();
					// $this->Sparepart_keluar_Model->statusdipinjam();
					$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
					redirect('sparepart_keluar','refresh');
				}
	}

	public function update($kode_keluar)
	{
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_masuk', 'Kode_masuk', 'trim|required');
		$this->form_validation->set_rules('tanggal_keluar', 'Tanggal_keluar', 'trim|required');
		// $this->form_validation->set_rules('nama_sparepart_masuk', 'Nama_sparepart_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');




		
		$this->load->model('Sparepart_Keluar_Model');
		$data['sparepart_keluar']=$this->Sparepart_Keluar_Model->getSparepartKeluar($kode_keluar);
		// $data["penerbit_list"] = $this->buku_model->getPenerbit();
		// $data["pengarang_list"] = $this->buku_model->getPengarang();	
		//skeleton code
		if($this->form_validation->run()==FALSE){

		//setelah load data dikirim ke view
			$this->load->view('edit_sparepart_keluar_view',$data);

		}else{
			$this->Sparepart_Keluar_Model->UpdateByKode($kode_keluar);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('sparepart_keluar/index');

		}
	}

	// public function kembalikan($id_peminjaman)
	// {
	// 	//load libraryl
	// 	$this->load->helper('url','form');	
	// 	$this->load->library('form_validation');
	// 	$this->form_validation->set_rules('id_peminjaman', 'IdPinjam', 'trim|required');
	// 	$this->form_validation->set_rules('id_user', 'Nim', 'trim|required');
	// 	$this->form_validation->set_rules('id_buku', 'Buku', 'trim|required');
	// 	$this->form_validation->set_rules('jatuhtempo', 'Tempo', 'trim|required');
	// 	$this->form_validation->set_rules('denda', 'Denda', 'trim|required');
	// 	$this->form_validation->set_rules('tanggal_kembali', 'TglKembali', 'trim|required');



		
		//sebelum update data harus ambil data lama yang akan di update
		// $this->load->model('Sparepart_keluar_Model');
		// $data['peminjaman']=$this->Sparepart_keluar_Model->updatekembali($id_peminjaman);
		// $data["kategori_list"] = $this->Sparepart_keluar_Model->getKategori();
		// $data["penerbit_list"] = $this->Sparepart_keluar_Model->getPenerbit();
		// $data["pengarang_list"] = $this->Sparepart_keluar_Model->getPengarang();	
		// //skeleton code
		// if($this->form_validation->run()==FALSE){

		// //setelah load data dikirim ke view
		// 	$this->load->view('pengembalian',$data);

		// }else{
		// 	$this->Sparepart_keluar_Model->updateById($id_peminjaman);
		// 	$this->load->view('edit_peminjaman_sukses');
		// }
	

	public function delete($kode_keluar)
 	{ 
 	 	$this->load->model('Sparepart_Keluar_Model');
  		$this->Sparepart_Keluar_Model->deleteByKode($kode_keluar);
 	 	redirect('sparepart_keluar');
	 }

}

/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */