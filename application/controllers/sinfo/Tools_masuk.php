<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_masuk extends CI_Controller {

	public function index()
	{
		$this->load->model('tools_masuk_model');
		$data["tools_masuk_list"] = $this->tools_masuk_model->getDataToolsMasuk();
		$this->load->view('sinfo/tools/v_tools_masuk', $data);		
	}

	public function datatable()
	{
		$this->load->model('tools_masuk_model');
		$data["tools_masuk_list"] = $this->tools_masuk_model->getDataToolsMasuk();
		$this->load->view('sinfo/tools/v_tools_masuk', $data);	
	}
		public function create()
	{

		$this->load->model('Tools_Masuk_Model');
		
		$data["tools_masuk_list"] = $this->Tools_Masuk_Model->getDataToolsMasuk();
		$data["tools_list"] = $this->Tools_Masuk_Model->getTools();

		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('tanggal_masuk', 'Tanggal_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		// $this->form_validation->set_rules('nama_tools_masuk', 'nama_tools_masuk', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('sinfo/tools/tambah_tools_masuk', $data);
		}
				else{
					$this->Tools_Masuk_Model->insertToolsMasuk();
					// $this->tools_Masuk_Model->statusdipinjam();
					$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
					redirect('sinfo/tools_masuk','refresh');
				}
			}
}