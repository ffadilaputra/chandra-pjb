<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_Sparepart_Keluar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('sparepart_keluar_model');
	}

	
	public function index()
	{
		$this->load->model('Sparepart_Keluar_Model');
		$data["sparepart_keluar_list"] = $this->Sparepart_Keluar_Model->getDataSparepartKeluar();
		$this->load->view('laporan_sparepart_keluar',$data);
 
 header("Content-type: application/vnd-ms-word");
 
 header("Content-Disposition: attachment; filename=laporan_sparepart_keluar.doc");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
	}

	// public function export(){
	// 	$this->load->model('sparepart_masuk_model');
	// $data["sparepart_masuk_list"] = $this->sparepart_masuk_model->getDataSparepartMasuk();
	// 	$this->load->view('laporan_sparepart_masuk', $data);

 	


}
?>