<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart_Masuk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	
	public function index()
	{
		
		$this->load->model('Sparepart_Masuk_Model');
		$data["sparepart_masuk_list"] = $this->Sparepart_Masuk_Model->getDataSparepartMasuk();
		$data["sparepart_list"] = $this->Sparepart_Masuk_Model->getSparepart();
		$this->load->view('sparepart_masuk', $data);


	}	

	public function datatable()
	{
		$this->load->model('Sparepart_Masuk_Model');
		$data["sparepart_masuk_list"] = $this->Sparepart_Masuk_Model->getDataSparepartMasuk();
		$this->load->view('sparepart_masuk',$data);	
	}


	public function create()
	{

		$this->load->model('Sparepart_Masuk_Model');
		
		$data["sparepart_masuk_list"] = $this->Sparepart_Masuk_Model->getDataSparepartMasuk();
		$data["sparepart_list"] = $this->Sparepart_Masuk_Model->getSparepart();

		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('tanggal_masuk', 'Tanggal_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah_masuk', 'Jumlah_masuk', 'trim|required');
		// $this->form_validation->set_rules('nama_sparepart_masuk', 'nama_sparepart_masuk', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('tambah_sparepart_masuk_view', $data);
		}
				else{
					$this->Sparepart_Masuk_Model->insertSparepartMasuk();
					// $this->Sparepart_Masuk_Model->statusdipinjam();
					$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
					redirect('sparepart_masuk','refresh');
				}
			}

	public function update($kode_masuk)
	{
		//load libraryl
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_masuk', 'Kode_masuk', 'trim|required');
		$this->form_validation->set_rules('tanggal_masuk', 'Tanggal_masuk', 'trim|required');
		// $this->form_validation->set_rules('nama_sparepart_masuk', 'Nama_sparepart_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah_masuk', 'Jumlah_masuk', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');




		
		$this->load->model('Sparepart_Masuk_Model');
		$data['sparepart_masuk']=$this->Sparepart_Masuk_Model->getSparepartMasuk($kode_masuk);
		
		if($this->form_validation->run()==FALSE){

		//setelah load data dikirim ke view
			$this->load->view('edit_sparepart_masuk_view',$data);

		}else{
			$this->Sparepart_Masuk_Model->UpdateByKode($kode_masuk);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('sparepart_masuk/index');

		}
	}
	

	public function delete($kode_masuk)
 	{ 
 	 	$this->load->model('Sparepart_Masuk_Model');
  		$this->Sparepart_Masuk_Model->deleteByKode($kode_masuk);
 	 	redirect('sparepart_masuk');
	 }

	public function updatestatus($kode_masuk)
	{
		//load libraryl
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_masuk', 'Kode_masuk', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		// $this->load->model('Sparepart_Masuk_Model');
		// $data['sparepart_masuk']=$this->Sparepart_Masuk_Model->getSparepartMasuk($kode_masuk);
		
		// if($this->form_validation->run()==FALSE){

		// //setelah load data dikirim ke view
		// 	redirect('sparepart_masuk/index');

		// }else{
			$this->load->model('Sparepart_Masuk_Model');
			$this->Sparepart_Masuk_Model->UpdateByStatus($kode_masuk);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('sparepart_masuk/index');
// }
		
	}



   
}

/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */