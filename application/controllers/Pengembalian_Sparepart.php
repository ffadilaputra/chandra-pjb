<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian_Sparepart extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function index()
	{
	$this->load->model('Peminjaman_Sparepart_Model');
	$data["pengembalian_list"] = $this->Peminjaman_Sparepart_Model->getDataPengembalian();
	$this->load->view('pengembalian_sparepart',$data);
	
	}	

	public function datatable()
	{
		$this->load->model('pengembalian_sparepart_model');
		$data["pengembalian_list"] = $this->pengembalian_sparepart_model->getDataPengembalian();
		$this->load->view('pengembalian_sparepart',$data);	
	}

	public function insertPengembalian()
	{

		$this->load->model('Pengembalian_Sparepart_Model');

		$this->load->helper('url','form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('tgl_peminjaman', 'Tgl_peminjaman', 'trim|required');
		$this->form_validation->set_rules('nama_sparepart', 'Nama_sparepart', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('tanggal_kembali', 'Tanggal_kembali', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		$this->load->model('pengembalian_sparepart_model');
		$this->pengembalian_sparepart_model->insertPengembalian();
		$this->pengembalian_saprepart_model->updatestatussparepart();
		$this->pengembalian_saprepart_model->updatestatuspeminjaman();
		redirect('pengembalian_sparepart','refresh');
			}
	}
