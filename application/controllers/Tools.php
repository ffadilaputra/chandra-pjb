<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->load->model('Tools_Model');
		$data["tools_list"] = $this->Tools_Model->getDataTools();
		$this->load->view('tools',$data);		
	}

		public function datatable()
	{
		$this->load->model('Tools_Model');
		$data["tools_list"] = $this->Tools_Model->getDataTools();
		$this->load->view('tools',$data);	
	}

	public function create()
	{
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_tools', 'Nama_tools', 'trim|required');	
		// $this->form_validation->set_rules('jumlah_tools', 'Jumlah_tools', 'trim|required');	
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');	
		$this->load->model('Tools_Model');	
		if($this->form_validation->run()==FALSE){
			$this->load->view('tambah_tools_view');
		}
			else
			{
				$this->Tools_Model->insertTools();
				$this->session->set_flashdata('pesan','tambah data berhasil');
				redirect('tools/index');

			}
	}

	public function update($kode_tools)
	{
		//load library
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_tools', 'Nama_tools', 'trim|required');	
		// $this->form_validation->set_rules('jumlah_tools', 'Jumlah_tools', 'trim|required');	
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');	
		
		//sebelum update data harus ambil data lama yang akan di update
		$this->load->model('Tools_Model');
		$data['tools']=$this->Tools_Model->getTools($kode_tools);
		//skeleton code
		if($this->form_validation->run()==FALSE){

		//setelah load data dikirim ke view
			$this->load->view('edit_tools_view',$data);

		}else{
			$this->Tools_Model->updateByKode($kode_tools);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('tools/index');

		}
	}
	public function delete($kode_tools)
	{
		$this->load->model('Tools_Model');
		$this->Tools_Model->deleteByKode($kode_tools);
		redirect('tools');
	}

}

