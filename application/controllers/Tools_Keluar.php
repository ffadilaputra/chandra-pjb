<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_Keluar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function datatable()
	{
			$this->load->model('Tools_Keluar_Model');
		$data["tools_keluar_list"] = $this->Tools_Keluar_Model->getDataToolsKeluar();
		$this->load->view('tools_keluar',$data);	
	}
	public function index()
	{
		/*$this->load->model('buku_model');
		$data["buku_list"] = $this->buku_model->getDataBuku();
		$this->load->view('buku',$data);*/
		$this->load->model('Tools_Keluar_Model');
		$data["tools_keluar_list"] = $this->Tools_Keluar_Model->getDataToolsKeluar();
		$data["tools_list"] = $this->Tools_Keluar_Model->getTools();
		
		// $data["pengarang_list"] = $this->Tools_keluar_Model->getPengarang();	
		$this->load->view('tools_keluar', $data);
	}	

	public function create()
	{

		$this->load->model('Tools_Keluar_Model');
		
		$data["tools_keluar_list"] = $this->Tools_Keluar_Model->getDataToolsKeluar();
		$data["tools_list"] = $this->Tools_Keluar_Model->getTools();

		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('tanggal_keluar', 'Tanggal_keluar', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		// $this->form_validation->set_rules('nama_tools_keluar', 'nama_tools_keluar', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('tambah_tools_keluar_view', $data);
		}
			
				// $this->load->model('Tools_keluar_Model');

				
				// $this->load->view('tambah_peminjaman_sukses');

				// $overload = $this->Tools_keluar_Model->Max();
				// if ($overload>=5) {
				// 	$this->session->set_flashdata('overload', 'sudah mencapai batas peminjaman 5 peminjaman');
				// 	$this->load->view('tambah_peminjaman_view', $data);
				
				else{
					$this->Tools_Keluar_Model->insertToolsKeluar();
					// $this->Tools_keluar_Model->statusdipinjam();
					$this->session->set_flashdata('berhasil', 'berhasil ditambahkan');
					redirect('tools_keluar','refresh');
				}
	}

	public function update($kode_keluar)
	{
		$this->load->helper('url','form');	
		$this->load->library('form_validation');
		// $this->form_validation->set_rules('kode_masuk', 'Kode_masuk', 'trim|required');
		$this->form_validation->set_rules('tanggal_keluar', 'Tanggal_keluar', 'trim|required');
		// $this->form_validation->set_rules('nama_tools_masuk', 'Nama_tools_masuk', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('distributor', 'Distributor', 'trim|required');




		
		$this->load->model('Tools_Keluar_Model');
		$data['tools_keluar']=$this->Tools_Keluar_Model->getToolsKeluar($kode_keluar);
		// $data["penerbit_list"] = $this->buku_model->getPenerbit();
		// $data["pengarang_list"] = $this->buku_model->getPengarang();	
		//skeleton code
		if($this->form_validation->run()==FALSE){

		//setelah load data dikirim ke view
			$this->load->view('edit_tools_keluar_view',$data);

		}else{
			$this->Tools_Keluar_Model->UpdateByKode($kode_keluar);
			$this->session->set_flashdata('pesan','update data berhasil');
			redirect('tools_keluar/index');

		}
	}

	// public function kembalikan($id_peminjaman)
	// {
	// 	//load libraryl
	// 	$this->load->helper('url','form');	
	// 	$this->load->library('form_validation');
	// 	$this->form_validation->set_rules('id_peminjaman', 'IdPinjam', 'trim|required');
	// 	$this->form_validation->set_rules('id_user', 'Nim', 'trim|required');
	// 	$this->form_validation->set_rules('id_buku', 'Buku', 'trim|required');
	// 	$this->form_validation->set_rules('jatuhtempo', 'Tempo', 'trim|required');
	// 	$this->form_validation->set_rules('denda', 'Denda', 'trim|required');
	// 	$this->form_validation->set_rules('tanggal_kembali', 'TglKembali', 'trim|required');



		
		//sebelum update data harus ambil data lama yang akan di update
		// $this->load->model('Sparepart_keluar_Model');
		// $data['peminjaman']=$this->Sparepart_keluar_Model->updatekembali($id_peminjaman);
		// $data["kategori_list"] = $this->Sparepart_keluar_Model->getKategori();
		// $data["penerbit_list"] = $this->Sparepart_keluar_Model->getPenerbit();
		// $data["pengarang_list"] = $this->Sparepart_keluar_Model->getPengarang();	
		// //skeleton code
		// if($this->form_validation->run()==FALSE){

		// //setelah load data dikirim ke view
		// 	$this->load->view('pengembalian',$data);

		// }else{
		// 	$this->Sparepart_keluar_Model->updateById($id_peminjaman);
		// 	$this->load->view('edit_peminjaman_sukses');
		// }
	

	public function delete($kode_keluar)
 	{ 
 	 	$this->load->model('Tools_Keluar_Model');
  		$this->Tools_Keluar_Model->deleteByKode($kode_keluar);
 	 	redirect('tools_keluar');
	 }

}

/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */