<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart_Keluar_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getDataSparepartKeluar()
		{$this->db->select("sparepart_keluar.kode_keluar,data_sparepart.kode_sparepart,data_sparepart.nama_sparepart,sparepart_keluar.tanggal_keluar,sparepart_keluar.jumlah ,sparepart_keluar.distributor");

				$this->db->join('data_sparepart','data_sparepart.kode_sparepart=sparepart_keluar.id_sparepart','data_sparepart.nama_sparepart=sparepart_keluar.nama_sparepart_keluar','left');
			
			$query = $this->db->get('sparepart_keluar');
			return $query->result();
		}

	public function insertSparepartKeluar()
		{
			$object = array(
				'id_sparepart' => $this->input->post('kode_sparepart'),
				'nama_sparepart_keluar' => $this->input->post('kode_sparepart'),
				'tanggal_keluar' => $this->input->post('tanggal_keluar'),
				'jumlah' => $this->input->post('jumlah'),
				'distributor' => $this->input->post('distributor'),			 
				);
			$this->db->insert('sparepart_keluar', $object);
		}

			public function getSparepartKeluar($kode_keluar)
		{
			$this->db->where('kode_keluar', $kode_keluar);	
			$query = $this->db->get('sparepart_keluar',1);
			return $query->result();

		}

		
		public function getSparepart()
		{
			$this->db->select("kode_sparepart,nama_sparepart,deskripsi_sparepart");
			$query=$this->db->get('data_sparepart');
			return $query->result();
		}

		public function updateByKode($kode_keluar)
		{
			
				$data = array('tanggal_keluar' => $this->input->post('tanggal_keluar'),
				'jumlah' => $this->input->post('jumlah'), 
				'distributor' => $this->input->post('distributor') );
			$this->db->where('kode_keluar', $kode_keluar);
			$this->db->update('sparepart_keluar', $data);


		}
		public function deleteByKode($kode_keluar)
		{
			$this->db->where('kode_keluar', $kode_keluar);
			$this->db->delete('sparepart_keluar');
		}
	}

