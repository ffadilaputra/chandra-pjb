<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_Keluar_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getDataToolsKeluar()
		{$this->db->select("tools_keluar.kode_keluar,data_tools.kode_tools,data_tools.nama_tools,tools_keluar.tanggal_keluar,tools_keluar.jumlah ,tools_keluar.distributor");

				$this->db->join('data_tools','data_tools.kode_tools=tools_keluar.id_tools','data_tools.nama_tools=tools_keluar.nama_tool','left');
			
			$query = $this->db->get('tools_keluar');
			return $query->result();
		}

	public function insertToolsKeluar()
		{
			$object = array(
				'id_tools' => $this->input->post('kode_tools'),
				'nama_tool' => $this->input->post('kode_tools'),
				'tanggal_keluar' => $this->input->post('tanggal_keluar'),
				'jumlah' => $this->input->post('jumlah'),
				'distributor' => $this->input->post('distributor'),			 
				);
			$this->db->insert('tools_keluar', $object);
		}

			public function getToolsKeluar($kode_keluar)
		{
			$this->db->where('kode_keluar', $kode_keluar);	
			$query = $this->db->get('tools_keluar',1);
			return $query->result();

		}

		
		public function getTools()
		{
			$this->db->select("kode_tools,nama_tools,deskripsi");
			$query=$this->db->get('data_tools');
			return $query->result();
		}

		public function updateByKode($kode_keluar)
		{
			
				$data = array('tanggal_keluar' => $this->input->post('tanggal_keluar'),
				'jumlah' => $this->input->post('jumlah'), 
				'distributor' => $this->input->post('distributor') );
			$this->db->where('kode_keluar', $kode_keluar);
			$this->db->update('tools_keluar', $data);


		}
		public function deleteByKode($kode_keluar)
		{
			$this->db->where('kode_keluar', $kode_keluar);
			$this->db->delete('tools_keluar');
		}
	}

