<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_Masuk_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getDataToolsMasuk()
		{
			$this->db->select("tools_masuk.kode_masuk,tools_masuk.nama_tools_masuk,tools_masuk.tanggal_masuk,tools_masuk.jumlah ,tools_masuk.distributor,tools_masuk.status");

				
			$query = $this->db->get('tools_masuk');
			return $query->result();
		}

	public function insertToolsMasuk()
		{
		 $waktu = date('Y-m-d');
			$object = array(
				// 'id_sparepart' => $this->input->post('kode_sparepart'),
				'nama_tools_masuk' => $this->input->post('nama_tools_masuk'),
				'tanggal_masuk' => $waktu,
				'jumlah' => $this->input->post('jumlah'),
				'distributor' => $this->input->post('distributor'),	
				'status' => "Tidak Disetujui"	 
				);
			$this->db->insert('tools_masuk', $object);
		}

	public function getToolsMasuk($kode_masuk)
		{
			$this->db->where('kode_masuk', $kode_masuk);	
			$query = $this->db->get('tools_masuk',1);
			return $query->result();

		}

		public function getTools()
		{
			$this->db->select("kode_tools,nama_tools,deskripsi");
			$query=$this->db->get('data_tools');
			return $query->result();
		}

		public function UpdateByKode($kode_masuk)
		{
				$data = array('tanggal_masuk' => $this->input->post('tanggal_masuk'),'jumlah' => $this->input->post('jumlah'),'distributor' => $this->input->post('distributor') );
			$this->db->where('kode_masuk', $kode_masuk);
			$this->db->update('tools_masuk', $data);

		}
		public function deleteByKode($kode_masuk)
		{
			$this->db->where('kode_masuk', $kode_masuk);
			$this->db->delete('tools_masuk');
		}
		public function UpdateByJumlah($tools_masuk)
		{
				$data = array('jumlah' => $this->input->post('jumlah') );
			$this->db->where('kode_masuk', $kode_masuk);
			$this->db->update('tools_masuk', $data);

		}
		 function get_all()
    {
        $this->db->order_by($this->kode_masuk, $this->order);
        return $this->db->get($this->table)->result();
    }
    		public function UpdateByStatus($kode_masuk)
		{
			$status=$this->input->post('status');
			$arrayName = array(
				'status' => 'Disetujui' );
			$this->db->where('kode_masuk', $kode_masuk );
			$this->db->update('tools_masuk', $arrayName);
		}

}

