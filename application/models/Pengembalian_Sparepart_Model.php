<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian_Sparepart_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	/*public function getDataBuku()
		{
			$this->db->select("id_buku,judul,kategori,penerbit,pengarang");
			$query = $this->db->get('buku');
			return $query->result();
		}*/
	public function getDataPengembalian()
		{
			// $this->db->select("pengembalian_sparepart.kode_kembali,pengembalian_sparepart.jumlah,pengembalian_sparepart.status,pengembalian_sparepart.tanggal_kembali,

			// 	peminjaman_sparepart.kode_peminjaman,peminjaman_sparepart.tgl_peminjaman,

			// 	datauser.id_user,datauser.nama,
				
			// 	data_sparepart.kode_sparepart,data_sparepart.nama_sparepart");

			// $this->db->join('peminjaman_sparepart','peminjaman_sparepart.kode_peminjaman=pengembalian_sparepart.id_peminjaman','peminjaman_sparepart.tgl_peminjaman=pengembalian_sparepart.tgl_pinjam','','left');

			// $this->db->join('datauser','datauser.id_user=pengembalian_sparepart.id_peminjam','datauser.nama=pengembalian.nama_peminjam','left');
		
			// $this->db->join('data_sparepart','data_sparepart.kode_sparepart=pengembalian_sparepart.id_sparepart' ,'data_sparepart.nama_sparepart=pengembalian.nama_sparepart_kembali','left');

			


			$query = $this->db->get('pengembalian_sparepart');
			return $query->result();
		}

	public function getDataPeminjamanById($kode_peminjaman)
		{
			$this->db->select("pengembalian_sparepart.kode_kembali,pengembalian_sparepart.jumlah,pengembalian_sparepart.status,pengembalian_sparepart.tanggal_kembali,

				peminjaman_sparepart.kode_peminjaman,peminjaman_sparepart.tgl_peminjaman,

				datauser.id_user,datauser.nama,
				
				data_sparepart.kode_sparepart,data_sparepart.nama_sparepart");

			$this->db->join('peminjaman_sparepart','peminjaman_sparepart.kode_peminjaman=pengembalian_sparepart.id_peminjaman','peminjaman_sparepart.tgl_peminjaman=pengembalian_sparepart.tgl_pinjam','','left');

			$this->db->join('datauser','datauser.id_user=pengembalian_sparepart.id_peminjam','datauser.nama=pengembalian.nama_peminjam','left');
		
			$this->db->join('data_sparepart','data_sparepart.kode_sparepart=pengembalian_sparepart.id_sparepart' ,'data_sparepart.nama_sparepart=pengembalian.nama_sparepart_kembali','left');
			$this->db->where('kode_peminjaman', $kode_peminjaman);
			$query = $this->db->get('peminjaman_sparepart');
			return $query->result();
		}



	// public function getPengarang()
	// 	{
	// 		$this->db->select("id_pengarang, nama_pengarang");
	// 		$query=$this->db->get('pengarang');
	// 		return $query->result();
	// 	}

	public function getpeminjaman()
		{
			$this->db->select("id_peminjaman, tgl_peminjaman");
			$this->db->where('status', 'tidakdipinjam');
			$query=$this->db->get('peminjaman_sparepart');
			return $query->result();
		}

	public function getDatauser()
		{
			$this->db->select("id_user,nama");
			$query=$this->db->get('datauser');
			return $query->result();
		}

	public function statusdipinjam()
		{
			$kode_sparepart=$this->input->post('kode_sparepart');
			$arrayName = array(	
				'status' => 'dipinjam' , );
			$this->db->where('kode_sparepart', $kode_sparepart);
			$this->db->update('data_sparepart', $arrayName);
		}

	public function statusbelumkembali()
		{
			$kode_sparepart=$this->input->post('kode_peminjaman');
			$arrayName = array(
				'status' => 'belumkembali' , );
			$this->db->where('kode_peminjaman', $kode_peminjaman );
			$this->db->update('peminjaman_sparepart', $arrayName);
		}

	/*public function getDataKategori()
		{
			$this->db->select("id_kategori,nama_kategori");
			$query = $this->db->get('kategori');
			return $query->result();
		}*/	

	public function insertPengembalian()
		{
			$object = array(
				'id_peminjaman' => $this->input->post('id_peminjaman'),
				'id_peminjam' => $this->input->post('id_peminjam'),
				'id_sparepart' => $this->input->post('id_sparepart'),
				'jumlah' => $this->input->post('jumlah'),
				'tanggal_kembali' => $this->input->post('tanggal_kembali'),
				'status' => "telahkembali"
				);
			$this->db->insert('pengembalian_sparepart', $object);
		}

		// public function getBuku($id_buku)
		// {
		// 	$this->db->where('id_buku', $id_buku);	
		// 	$query = $this->db->get('buku',1);
		// 	return $query->result();

		// }

		public function updatestatuspeminjaman()
		{
			$id_peminjaman=$this->input->post('kode_peminjaman');
			$object = array(
				'status' => "TELAHKEMBALI" //update ke status peminjaman
				);
			$this->db->where('kode_peminjaman', $kode_peminjaman);
			$this->db->update('peminjaman_sparepart', $object);
		}
		public function updatestatussparepart()
		{
			$kode_sparepart=$this->input->post('kode_sparepart');
			$object = array(
				'status' => "tidakdipinjam" //update ke status buku
				);
			$this->db->where('kode_sparepart', $kode_sparepart);
			$this->db->update('data_sparepart', $object);
		}

		public function updateById($kode_peminjaman)
		{
			$data = array(
				'id_peminjaman' => $this->input->post('id_peminjaman'),
				'id_peminjam' => $this->input->post('id_peminjam'),
				'id_sparepart' => $this->input->post('id_sparepart'),
				'jumlah' => $this->input->post('jumlah'),
				'tanggal_kembali' => $this->input->post('tanggal_kembali'),
				'status' => "telahkembali"
				 );
			$this->db->where('kode_kembali', $kode_kembali);
			$this->db->update('pengembalian_sparepart', $data);

		}

		public function updatekembali($id_peminjaman)
		{
			$data = array(
				'id_peminjaman' => $this->input->post('id_peminjaman'),
				'Id_peminjam' => $this->input->post('id_user'),
				'Id_buku_dipinjam' => $this->input->post('id_buku'),
				'Jatuhtempo' => $this->input->post('jatuhtempo'),
				'Denda' => $this->input->post('denda'),
				'Tanggal_dikembalikan' => $this->input->post('tanggal_kembali')
				 );
			$this->db->where('kode_kembali', $kode_kembali);
			$this->db->update('pengembalian_sparepart', $data);

		}

		public function deleteById($id_peminjaman) 
		{
			$this->db->where('id_peminjaman',$id_peminjaman);
			$this->db->delete('peminjaman');
		}

		function Max()
		{
			$id_user=$this->input->post('id_user');
			
			$this->db->select('user.id_user');
			$this->db->from('peminjaman');
			$this->db->where('user.id_user',$id_user);
			$this->db->where('peminjaman.status',"belumkembali");
			$this->db->join('user','user.id_user=peminjaman.id_peminjam','left');
			$query=$this->db->count_all_results();
			return $query;
		}

}

/* End of file Buku_Model.php */
/* Location: ./application/models/Buku_Model.php */
