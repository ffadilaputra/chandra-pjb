<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembalian_Model extends CI_Model {

	/*public function getDataBuku()
		{
			$this->db->select("id_buku,judul,kategori,penerbit,pengarang");
			$query = $this->db->get('buku');
			return $query->result();
		}*/
	public function getDataPengembalian()
		{
			$this->db->select("pengembalian_sparepart.kode_kembali,peminjaman.id_peminjaman, peminjaman.tanggal_peminjaman, peminjaman.batas_kembali, pengembalian.status,
				user.nim, user.nama, 
				buku.id_buku , buku.judul, 
				pengembalian.tanggal_dikembalikan, pengembalian.denda, pengembalian.id_pengembalian");

			$this->db->join('peminjaman','peminjaman.id_peminjaman=pengembalian.id_pinjam' ,'peminjaman.tanggal_peminjaman=pengembalian.judul_bukupinjam','left');
			$this->db->join('buku','buku.id_buku=pengembalian.id_buku' ,'buku.judul=pengembalian.judul_bukupinjam','left');
			$this->db->join('user','user.id_user=pengembalian.id_user' ,'user.nama=pengembalian.nama_peminjam','left');
			
			$query = $this->db->get('pengembalian');
			return $query->result();
		}

	public function getDataPeminjamanById($id_peminjaman)
		{
			$this->db->select("peminjaman.id_peminjaman, peminjaman.tanggal_peminjaman, peminjaman.batas_kembali, peminjaman.status, user.nim, user.nama, buku.id_buku , buku.judul");

			$this->db->join('buku','buku.id_buku=peminjaman.id_buku_dipinjam' ,'buku.judul=peminjaman.judul_bukupinjam','left');
			$this->db->join('user','user.id_user=peminjaman.id_peminjam' ,'user.nama=peminjaman.nama_peminjam','left');
			$this->db->where('id_peminjaman', $id_peminjaman);
			$query = $this->db->get('peminjaman');
			return $query->result();
		}



	// public function getPengarang()
	// 	{
	// 		$this->db->select("id_pengarang, nama_pengarang");
	// 		$query=$this->db->get('pengarang');
	// 		return $query->result();
	// 	}

	public function getBuku()
		{
			$this->db->select("id_buku, judul");
			$this->db->where('status', 'tidakdipinjam');
			$query=$this->db->get('buku');
			return $query->result();
		}

	public function getNim()
		{
			$this->db->select("id_user, nim,nama");
			$query=$this->db->get('user');
			return $query->result();
		}

	public function statusdipinjam()
		{
			$id_buku=$this->input->post('id_buku');
			$arrayName = array(	
				'status' => 'dipinjam' , );
			$this->db->where('id_buku', $id_buku );
			$this->db->update('buku', $arrayName);
		}

	public function statusbelumkembali()
		{
			$id_buku=$this->input->post('id_peminjaman');
			$arrayName = array(
				'status' => 'belumkembali' , );
			$this->db->where('id_peminjaman', $id_peminjaman );
			$this->db->update('peminjaman', $arrayName);
		}

	/*public function getDataKategori()
		{
			$this->db->select("id_kategori,nama_kategori");
			$query = $this->db->get('kategori');
			return $query->result();
		}*/	

	public function insertPengembalian()
		{
			$object = array(
				'id_peminjaman' => $this->input->post('id_peminjaman'),
				'id_peminjam' => $this->input->post('id_peminjam'),
				'id_sparepart' => $this->input->post('id_sparepart'),
				'jumlah' => $this->input->post('jumlah'),
				'tanggal_kembali' => $this->input->post('tanggal_kembali'),
				'status' => "telahkembali"
				);
			$this->db->insert('pengembalian_sparepart', $object);
		}

		// public function getBuku($id_buku)
		// {
		// 	$this->db->where('id_buku', $id_buku);	
		// 	$query = $this->db->get('buku',1);
		// 	return $query->result();

		// }

		public function updatestatuspeminjaman()
		{
			$id_peminjaman=$this->input->post('id_peminjaman');
			$object = array(
				'status' => "TELAHKEMBALI" //update ke status peminjaman
				);
			$this->db->where('id_peminjaman', $id_peminjaman);
			$this->db->update('peminjaman', $object);
		}
		public function updatestatusbuku()
		{
			$id_buku=$this->input->post('id_buku');
			$object = array(
				'status' => "tidakdipinjam" //update ke status buku
				);
			$this->db->where('id_buku', $id_buku);
			$this->db->update('buku', $object);
		}

		public function updateById($kode_peminjaman)
		{
			$data = array(
				'id_peminjaman' => $this->input->post('id_peminjaman'),
				'Id_peminjam' => $this->input->post('id_user'),
				'Tanggal_peminjaman' => $this->input->post('tanggal_peminjaman'),
				'Id_buku_dipinjam' => $this->input->post('id_buku'),
				'Batas_kembali' => $this->input->post('batas_kembali')
				 );
			$this->db->where('id_kembali', $id_kembali);
			$this->db->update('pengembalian', $data);

		}

		public function updatekembali($id_peminjaman)
		{
			$data = array(
				'id_peminjaman' => $this->input->post('id_peminjaman'),
				'Id_peminjam' => $this->input->post('id_user'),
				'Id_buku_dipinjam' => $this->input->post('id_buku'),
				'Jatuhtempo' => $this->input->post('jatuhtempo'),
				'Denda' => $this->input->post('denda'),
				'Tanggal_dikembalikan' => $this->input->post('tanggal_kembali')
				 );
			$this->db->where('id_kembali', $id_kembali);
			$this->db->update('pengembalian', $data);

		}

		public function deleteById($id_peminjaman) 
		{
			$this->db->where('id_peminjaman',$id_peminjaman);
			$this->db->delete('peminjaman');
		}

		function Max()
		{
			$id_user=$this->input->post('id_user');
			
			$this->db->select('user.id_user');
			$this->db->from('peminjaman');
			$this->db->where('user.id_user',$id_user);
			$this->db->where('peminjaman.status',"belumkembali");
			$this->db->join('user','user.id_user=peminjaman.id_peminjam','left');
			$query=$this->db->count_all_results();
			return $query;
		}

}

/* End of file Buku_Model.php */
/* Location: ./application/models/Buku_Model.php */
