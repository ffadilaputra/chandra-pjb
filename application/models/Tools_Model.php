<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getDataTools()
		{
			$this->db->select("kode_tools,nama_tools,deskripsi");
			$query = $this->db->get('data_tools');
			return $query->result();
		}

	public function insertTools()
		{
			$object = array(
				'kode_tools' => $this->input->post('kode_tools'),
				'nama_tools' => $this->input->post('nama_tools'),	
				'deskripsi' => $this->input->post('deskripsi'),			 
				);
			$this->db->insert('data_tools', $object);
		}

		public function getTools($kode_tools)
		{
			$this->db->where('kode_tools', $kode_tools);	
			$query = $this->db->get('data_tools',1);
			return $query->result();

		}

		public function updateByKode($kode_tools)
		{
			$data = array('nama_tools' => $this->input->post('nama_tools'),'deskripsi' => $this->input->post('deskripsi') );
			$this->db->where('kode_tools', $kode_tools);
			$this->db->update('data_tools', $data);

		}
		public function deleteByKode($kode_tools)
		{
			$this->db->where('kode_tools', $kode_tools);
			$this->db->delete('data_tools');
		}
	}

