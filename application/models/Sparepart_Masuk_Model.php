<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart_Masuk_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getDataSparepartMasuk()
		{
			$this->db->select("sparepart_masuk.kode_masuk,sparepart_masuk.nama_sparepart_masuk,sparepart_masuk.tanggal_masuk,sparepart_masuk.jumlah_masuk ,sparepart_masuk.distributor,sparepart_masuk.status");

				// $this->db->join('data_sparepart','data_sparepart.kode_sparepart=sparepart_masuk.id_sparepart','data_sparepart.nama_sparepart=sparepart_masuk.nama_sparepart_masuk','left');
			
			$query = $this->db->get('sparepart_masuk');
			return $query->result();
		}
		

	public function insertSparepartMasuk()
		{
		 $waktu = date('Y-m-d');
			$object = array(
				// 'id_sparepart' => $this->input->post('kode_sparepart'),
				'nama_sparepart_masuk' => $this->input->post('nama_sparepart_masuk'),
				'tanggal_masuk' => $waktu,
				'jumlah_masuk' => $this->input->post('jumlah_masuk'),
				'distributor' => $this->input->post('distributor'),	
				'status' => "Tidak Disetujui"	 
				);
			$this->db->insert('sparepart_masuk', $object);
		}

	public function getSparepartMasuk($kode_masuk)
		{
			$this->db->where('kode_masuk', $kode_masuk);	
			$query = $this->db->get('sparepart_masuk',1);
			return $query->result();

		}

		public function getSparepart()
		{
			$this->db->select("kode_sparepart,nama_sparepart,deskripsi_sparepart");
			$query=$this->db->get('data_sparepart');
			return $query->result();
		}

		public function UpdateByKode($kode_masuk)
		{
				$data = array('tanggal_masuk' => $this->input->post('tanggal_masuk'),'jumlah_masuk' => $this->input->post('jumlah_masuk'),'distributor' => $this->input->post('distributor') );
			$this->db->where('kode_masuk', $kode_masuk);
			$this->db->update('sparepart_masuk', $data);

		}
		public function deleteByKode($kode_masuk)
		{
			$this->db->where('kode_masuk', $kode_masuk);
			$this->db->delete('sparepart_masuk');
		}
		public function UpdateByJumlah($sparepart_masuk)
		{
				$data = array('jumlah_masuk' => $this->input->post('jumlah_masuk') );
			$this->db->where('kode_masuk', $kode_masuk);
			$this->db->update('sparepart_masuk', $data);

		}
		 function get_all()
    {
        $this->db->order_by($this->kode_masuk, $this->order);
        return $this->db->get($this->table)->result();
    }
    		public function UpdateByStatus($kode_masuk)
		{
			$status=$this->input->post('status');
			$arrayName = array(
				'status' => 'Disetujui' );
			$this->db->where('kode_masuk', $kode_masuk );
			$this->db->update('sparepart_masuk', $arrayName);
		}

}

