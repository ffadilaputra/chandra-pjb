<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman_Sparepart_Model extends CI_Model {
	
	public function getDataPeminjaman()
		{

			$status = 'dipinjam';

			$this->db->select('a.kode_peminjaman , b.nama , c.nama_sparepart_masuk , a.jumlah , a.tanggal_peminjaman , a.tanggal_pengembalian , a.status' );
			$this->db->from('peminjaman_sparepart as a ');
			$this->db->join('datauser as b', 'b.id_user = a.user_id');
			$this->db->join('sparepart_masuk as c', 'c.kode_masuk = a.sparepart_masuk_id');
			$this->db->where('a.status',$status);
			$query = $this->db->get();
			return $query->result();
		}

		public function getDataPengembalian()
		{
			$status = 'kembali';
			$this->db->select('a.sparepart_masuk_id,a.tanggal_peminjaman,a.kode_peminjaman , b.nama , c.nama_sparepart_masuk , a.jumlah , a.tanggal_peminjaman , a.tanggal_pengembalian , a.status' );
			$this->db->from('peminjaman_sparepart as a ');
			$this->db->join('datauser as b', 'b.id_user = a.user_id');
			$this->db->join('sparepart_masuk as c', 'c.kode_masuk = a.sparepart_masuk_id');
			$this->db->where('a.status',$status);
			$query = $this->db->get();
			return $query->result();
		}	


	public function getDataPeminjamanById($kode_peminjaman)
		{
			$this->db->select("peminjaman_sparepart.kode_peminjaman,peminjaman_sparepart.jumlah, peminjaman_sparepart.tanggal_peminjaman,peminjaman_sparepart.tanggal_pengembalian, peminjaman_sparepart.status, data_sparepart.kode_sparepart, data_sparepart.nama_sparepart,datauser.nama , datauser.bagian");

			$this->db->join('datauser','datauser.id_user=peminjaman_sparepart.user_id' ,'datauser.nama=peminjaman_sparepart.nama_peminjam','left');
			$this->db->join('data_sparepart','data_sparepart.kode_sparepart=peminjaman_sparepart.sparepart_masuk_id','left');
			$this->db->where('peminjaman_sparepart.status', "dipinjam");
			
			$this->db->where('kode_peminjaman', $kode_peminjaman);
			$query = $this->db->get('peminjaman_sparepart');
			return $query->result();
		}


	public function getMasuk()
		{
			$this->db->select("kode_masuk, jumlah_masuk, nama_sparepart_masuk");
			$query=$this->db->get('sparepart_masuk');
			return $query->result();
		}
	public function getSparepart()
		{
			$this->db->select("kode_sparepart, nama_sparepart");
			$query=$this->db->get('data_sparepart');
			return $query->result();
		}

	public function getUser()
		{
			$this->db->select("id_user,nama");
			$query=$this->db->get('datauser');
			return $query->result();
		}
	
	
	public function insertPeminjaman()
		{
			$object = array(
				// 'kode_peminjaman' => $this->input->post('kode_sparepart'),
				'user_id' => $this->input->post('id_user'),
				'jumlah' => $this->input->post('jumlah'),
				'tanggal_peminjaman' => date('Y-m-d'),
				'tanggal_pengembalian' => $batas=date('Y-m-d', strtotime($batas.'+ 3 days')),
				'status' => "Dipinjam"

				);
		// if ('jumlah_masuk') {
		// 	"'jumlah_masuk'- 'jumlah' = 'jumlah_masuk'";
		// }
			$this->db->insert('peminjaman_sparepart', $object);
		
		}

		// public function getBuku($id_buku)
		// {
		// 	$this->db->where('id_buku', $id_buku);	
		// 	$query = $this->db->get('buku',1);
		// 	return $query->result();

		// }

		public function updateById($kode_peminjaman)
		{
			$data = array(

				'id_peminjam' => $this->input->post('id_pengembalian'),
				'id_peminjam' => $this->input->post('nama_peminjam'),
				'id_sparepart_dipinjam' => $this->input->post('nama_sparepart'),
				'jumlah' => $this->input->post('jumlah'),
				'tgl_peminjaman' => $this->input->post('tanggal_dikembalikan'),
				'batas_pengembalian' => $this->input->post('batas_pengembalian'),
				'status' => "telahkembali");
			$this->db->where('kode_peminjaman', $kode_peminjaman );
			$this->db->update('peminjaman_sparepart', $data);

		}

		public function updatekembali($kode_peminjaman)
		{
			$data = array(
				
				'id_peminjaman' => $this->input->post('id_pengembalian'),
				'id_peminjam' => $this->input->post('nama_peminjam'),
				'id_sparepart' => $this->input->post('nama_sparepart'),
				'jumlah' => $this->input->post('jumlah'),
				'tanggal_dikembalikan' => $this->input->post('tanggal_dikembalikan'),
				'status' => "telahkembali");
			$this->db->where('kode_peminjaman', $kode_peminjaman );
			$this->db->update('pengembalian_sparepart', $data);


		}

		public function deleteByKode($kode_peminjaman) 
		{
			$this->db->where('kode_peminjaman',$kode_peminjaman);
			$this->db->delete('peminjaman_sparepart');
		}

		public function getKeranjang()
		{
		$query = $this->db->query("SELECT*FROM keranjang");
			return $query->result_array();
		}
			function tampilKeranjang(){
        return $this->db->get("keranjang");
    	}
    	public function truncateKeranjang()
		{
			$this->db->truncate('keranjang');
		}
		 		function simpan($info){
        $this->db->insert("detail",$info);
    }
    	public function insertKeranjang()
 		{
  			$object = array(
  				'sparepart_id' => $this->input->post('kode_sparepart'), 
  				'judul' => $this->input->post('judul'), 
  				'tanggal_pinjam' => $this->input->post('tanggal_pinjam'), 
  				'jatuh_tempo' => $this->input->post('jatuh_tempo'),
  				'jumlah' => $this->input->post('jumlah')

  				);
  			$this->db->insert('keranjang', $object);
		 }
		 
		 public function pengembalianBarang($id){
			$data = array('status' => 'kembali', 'tanggal_pengembalian' => date('Y-m-d'));
			$this->db->where('kode_peminjaman',$id);
			$this->db->update('peminjaman_sparepart',$data);
		 }

}
