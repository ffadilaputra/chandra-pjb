<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
	public function daftar($data)
       {
            $this->db->insert('datauser',$data);
       }
	public function login($username,$password)
	{
		$this->db->select('id_user,nama,bagian,username,password,status');
		$this->db->from('datauser');
		$this->db->where('username', $username);
		$this->db->where('password', MD5($password));
		$query = $this->db->get();
		if($query->num_rows()==1){
			return $query->result();
		}else{
			return false;
		}
	}

	public function getDataUser()
		{
			$this->db->select("datauser.id_user,datauser.nama,datauser.bagian,datauser.username ,datauser.password,datauser.status");

				
			$query = $this->db->get('datauser');
			return $query->result();
		}
	

}

/* End of file User.php */
/* Location: ./application/models/User.php */

 ?>