<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sparepart_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getDataSparepart()
		{
			$this->db->select("kode_sparepart,nama_sparepart,deskripsi_sparepart");
			$query = $this->db->get('data_sparepart');
			return $query->result();
		}

	public function insertSparepart()
		{
			$object = array(
				'kode_sparepart' => $this->input->post('kode_sparepart'),
				'nama_sparepart' => $this->input->post('nama_sparepart'),
				// 'jumlah_sparepart' => $this->input->post('jumlah_sparepart'),	
				'deskripsi_sparepart' => $this->input->post('deskripsi_sparepart'),				 			 
				);
			$this->db->insert('data_sparepart', $object);
		}

		public function getSparepart($kode_sparepart)
		{
			$this->db->where('kode_sparepart', $kode_sparepart);	
			$query = $this->db->get('data_sparepart',1);
			return $query->result();

		}

		public function updateByKode($kode_sparepart)
		{
			$data = array( 'nama_sparepart' => $this->input->post('nama_sparepart'), 'deskripsi_sparepart' => $this->input->post('deskripsi_sparepart') );
			$this->db->where('kode_sparepart', $kode_sparepart);
			$this->db->update('data_sparepart', $data);

		}
		public function deleteBykode($kode_sparepart)
		{
			$this->db->where('kode_sparepart', $kode_sparepart);
			$this->db->delete('data_sparepart');
		}
	}

