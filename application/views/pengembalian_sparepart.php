<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PLTU PAITON</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/bootstrap.min.css">
		 <link rel="stylesheet" href="<?php echo base_url('') ?>assets/datatables.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
	<div class="navbar navbar-default">
                <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Pengembalian Sparepart</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('home_admin');?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>

                     
                    </ul>

                        </form>
                    </div>

                </div><!--/.nav-collapse -->
                </div>


					
				</div>	
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<center><h1>Form Pengembalian Sparepart</h1></center>	
						<div class="table-responsive">
						
							<table class="table table-bordered table-hover" id="example">
								<thead>
									
									<tr>
										<th>No Kembali</th>
										<th>Nama Peminjam</th>
										<th>Tanggal Peminjam</th>
										<th>Nama Sparepart</th>
									 	<th>Jumlah</th>
										<th>Tanggal Dikembalikan</th> 
										<th>Status</th>  
									</tr>
								</thead>
								<tbody>
								<?php foreach ($pengembalian_list as $key) { ?>
									<tr>
										<!-- <td><?php echo $key->id_peminjaman ?></td> -->
										<td><?php echo $key->sparepart_masuk_id ?></td>
										<td><?php echo $key->nama ?></td>
										<td><?php echo $key->tanggal_peminjaman ?></td>
										<td><?php echo $key->nama_sparepart_masuk ?></td> 
										<td><?php echo $key->jumlah ?></td>
										<td><?php echo $key->tanggal_pengembalian ?></td>
										<td><?php echo $key->status ?></td>
									</tr>

								<?php } ?>
								
								</tbody>
							</table>
						</div>
					</div>
			



		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url('') ?>assets/datatables.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		 		<script>
            $(document).ready(function(){
                $('#example').DataTable();
            });
        </script>
	</body>
</html>
