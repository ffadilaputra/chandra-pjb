<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Updatar Sparepart</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="<?php echo base_url('') ?>assets/DataTables/datatables.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<!--header-->
<div class="navbar navbar-default">
                <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Polinema</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('home_admin');?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>
						 <li class="active"><a href="<?php echo site_url('sparepart');?>"><i class="glyphicon glyphicon-home"></i>Sparepart</a></li>
                     
                    </ul>

                    

                </div><!--/.nav-collapse -->
                </div>
            </div>
 

                    <!--LIST TABEL-->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<?php echo form_open('sparepart_masuk/update/'.$this->uri->segment(3)); ?>
								<legend>Edit Data Sparepart</legend>
								<?php echo validation_errors(); ?>
								<div class="form-group">
									<label for="">Tanggal Masuk Sparepart</label>
									<input type="date" class="form-control" id="tanggal_masuk" name="tanggal_masuk" placeholder="input field" value="<?php echo $sparepart_masuk[0]->tanggal_masuk ?>">
								</div>
								<div class="form-group">
									<label for="">Jumlah Sparepart Masuk</label>
									<input type="text" class="form-control" id="jumlah_masuk" name="jumlah_masuk" placeholder="input field" value="<?php echo $sparepart_masuk[0]->jumlah_masuk ?>">
								</div>
								<div class="form-group">
									<label for="">Distributor Sparepart</label>
									<input type="text" class="form-control" id="distributor" name="distributor" placeholder="input field" value="<?php echo $sparepart_masuk[0]->distributor ?>">
								</div>
								
								<br>
								<div>
									<button type="submit" class="btn btn-primary">Simpan</button>
									<?php echo form_close(); ?>
								</div>
	
	
								



		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="<?php echo base_url('') ?>assets/DataTables/datatables.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script>
            $(document).ready(function(){
                $('#example').DataTable();
            });
        </script>
	</body>
</html>