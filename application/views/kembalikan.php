<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PLTU PAITON</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/bootstrap.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container-fluid">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<nav class="navbar navbar-default" role="navigation">
							<div class="container-fluid">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<a class="navbar-brand" href="#">Title</a>
								</div>
						
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse navbar-ex1-collapse">
									<ul class="nav navbar-nav">
										<li class="active"><a href="<?php echo site_url('peminjaman') ?>">Data Peminjaman</a></li>
									</ul>
									<ul class="nav navbar-nav navbar-right">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu<b class="caret"></b></a>
											<ul class="dropdown-menu">
												<li><a href="#">Action</a></li>
												<li><a href="#">Another action</a></li>
												<li><a href="#">Something else here</a></li>
												<li><a href="#">Separated link</a></li>
											</ul>
										</li>
									</ul>
								</div><!-- /.navbar-collapse -->
						</div>
						</nav>

					</div>	

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<?php echo form_open('pengembalian/insertPengembalian/'); ?>
								<legend>Kembalikan Buku</legend>
								<?php echo validation_errors(); ?>
								<div class="table-responsive">
								<table class="table table-hover">
								
									<label  for="">No Peminjaman :</label>
									<?php echo $peminjaman[0]->id_peminjaman ?><br>
									<input type="hidden" name="id_peminjaman" value="<?php echo $peminjaman[0]->id_peminjaman ?>">
									
									<label  for="">Nim Peminjam :</label>
									<?php echo $peminjaman[0]->nim ?><br>
									<input type="hidden" name="id_user" value="<?php echo $peminjaman[0]->id_peminjam ?>">

									<label  for="">Nama Peminjam :</label>
									<?php echo $peminjaman[0]->nama ?><br>
									<input type="hidden" name="nama" value="<?php echo $peminjaman[0]->nama ?>">

									<label  for="">Id Buku Dipinjam :</label>
									<?php echo $peminjaman[0]->id_buku ?><br>
									<input type="hidden" name="id_buku" value="<?php echo $peminjaman[0]->id_buku ?>">

									<label  for="">Judul Buku :</label>
									<?php echo $peminjaman[0]->judul ?><br>
									<input type="hidden" name="judul" value="<?php echo $peminjaman[0]->judul ?>">

									<label  for="">Tgl Peminjaman :</label>
									<?php echo $peminjaman[0]->tanggal_peminjaman ?><br>
									<input type="hidden" name="tanggal_peminjaman" value="<?php echo $peminjaman[0]->tanggal_peminjaman ?>">

									<label  for="">Batas Kembali :</label>
									<?php echo $peminjaman[0]->batas_kembali ?><br>
									<input type="hidden" name="batas_kembali" value="<?php echo $peminjaman[0]->batas_kembali ?>">

									<label  for="">Status :</label>
									<?php echo $peminjaman[0]->status ?><br>
									<input type="hidden" name="status" value="<?php echo $peminjaman[0]->status ?>">

									<label for="">Tgl Kembali (Tgl hari ini) :</label>
									<?php echo date('Y-m-d'); ?>
									
										<input type="hidden" name="tanggal_kembali" id="tanggal_kembali" class="form-control" value="<?php echo date('Y-m-d'); 
										?>"><br>
									
									<label  for="">Terlambat (Hari) :</label>
									<?php 
									$datetime1 = strtotime($peminjaman[0]->batas_kembali);
									$datetime2 = strtotime(date('Y-m-d'));
									$datediff = $datetime2 - $datetime1;
									$jatuhtempo = floor($datediff/(60*60*24));
									if ($jatuhtempo <= 0) {
										# code...
										echo "0";
									}
									else {
										echo $jatuhtempo;
									}
									 ?>
									<input type="hidden" name="jatuhtempo" class="form-control" value="<?php 
									$datetime1 = strtotime($peminjaman[0]->batas_kembali);
									$datetime2 = strtotime(date('Y-m-d'));
									$datediff = $datetime2 - $datetime1;
									$jatuhtempo = floor($datediff/(60*60*24));
									if ($jatuhtempo <= 0) {
										# code...
										echo "0";
									}
									else {
										echo $jatuhtempo;
									}
									 ?>"><br>

									<label  for="">Denda :</label>
									<?php $denda = $jatuhtempo * 500 - 1500;
									 	if ($denda <= 0) {
									 	echo "Rp. 0";
									 	}
									 	else{
									 	echo "Rp. ".$denda;
									 	}

									 		?>
									<input type="hidden" class="form-control" name="denda" value="<?php $denda = $jatuhtempo * 500 - 1500;
									 	if ($denda <= 0) {
									 	echo "Rp. 0";
									 	}
									 	else{
									 	echo "Rp. ".$denda;
									 	}

									 		?>"><br>
									 	
								<button type="submit" class="btn btn-primary">Kembalikan !</button>
								</table>
								</div>
					<?php echo form_close(); ?>
					</div>


		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>