<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PLTU PAITON</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/bootstrap.min.css">
		 <link rel="stylesheet" href="<?php echo base_url('') ?>assets/datatables.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
	<div class="navbar navbar-default">
                <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Perpustakaan</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('home_admin');?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>

                     
                    </ul>

                    <div class="nav navbar-nav navbar-right">
                        <form class="navbar-form navbar-left">
                           <a href="<?php echo site_url('pengembalian/create/')?>" type="button" class="btn btn-info"><i class="glyphicon glyphicon-plus-sign"> </i>Tambah
                                    </a>
                  
                        </form>
                    </div>

                </div><!--/.nav-collapse -->
                </div>


					
				</div>	
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<center><h1>Form Pengembalian</h1></center>	
						<div class="table-responsive">
						
							<table class="table table-bordered table-hover" id="example">
								<thead>
									
									<tr>
										<th>No Kembali</th>
										<th>No Pinjam</th>
										<th>NIM</th>
										<th>Nama Peminjam</th>
									 	<th>ID Buku</th>
										<th>Judul</th> 
										<th>Tanggal Pinjam</th>
										<th>Batas Kembali</th>
										<th>Tanggal Dikembalikan</th>
										<th>Denda</th>  
										<th>Status</th>  
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($pengembalian_list as $key) { ?>
									<tr>
										<!-- <td><?php echo $key->id_peminjaman ?></td> -->
										<td><?php echo $key->id_pengembalian ?></td>
										<td><?php echo $key->id_peminjaman ?></td>
										<td><?php echo $key->nim ?></td>
										<td><?php echo $key->nama ?></td>
										<td><?php echo $key->id_buku ?></td>
										<td><?php echo $key->judul ?></td> 
										<td><?php echo $key->tanggal_peminjaman ?></td>
										<td><?php echo $key->batas_kembali ?></td>
										<td><?php echo $key->tanggal_dikembalikan ?></td>
										<td><?php echo $key->denda ?></td>
										<td><?php echo $key->status ?></td>
										<td>
										<!-- <a href="<?php echo site_url('peminjaman/update/').$key->id_peminjaman ?>" type="button" class="btn btn-info">Edit</a> -->
										<!-- <a href="<?php echo site_url('peminjaman/update/').$key->id_peminjaman ?>" type="button" class="btn btn-warning" ><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Pengembalian</a>
										<a href="<?php echo site_url('peminjaman/delete/').$key->id_peminjaman ?>" type="button" class="btn btn-danger" onClick="JavaScript: return confirm('Anda yakin ingin menghapus data ini?')"><span class="glyphicon glyphicon-trash" aria-hidden="true"> --></span></a>
										</td>
									</tr>

								<?php } ?>
								
								</tbody>
							</table>
						</div>
					</div>
			



		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url('') ?>assets/datatables.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		 		<script>
            $(document).ready(function(){
                $('#example').DataTable();
            });
        </script>
	</body>
</html>