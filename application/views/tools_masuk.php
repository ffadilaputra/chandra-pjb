<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PLTU PAITON</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/bootstrap.min.css">
		 <link rel="stylesheet" href="<?php echo base_url('') ?>assets/datatables.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
<div class="navbar navbar-default">
                <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Data Tools</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('home_admin');?>"><i class="glyphicon glyphicon-home"></i> Home</a></li>

                     
                    </ul>
                     <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('tools');?>"><i class="glyphicon glyphicon-book"></i> Tools</a></li>

                     
                    </ul>


                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('peminjaman_tools');?>"><i class="glyphicon glyphicon-book"></i> Peminjaman Tools</a></li>

                     
                    </ul>
                     <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('pengembalian_tools');?>"><i class="glyphicon glyphicon-book"></i> Pemngembalian Tools</a></li>

                     
                     
                    </ul>
                    </ul>
                    
					</ul>
					   <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('tools_keluar');?>"><i class="glyphicon glyphicon-book"></i> Tools Keluar</a></li>
                     
                    </ul>
                     <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('laporan_tools_masuk');?>"><i class="glyphicon glyphicon-print"></i> Laporan Tools Masuk</a></li>
                    </ul>


                    <div class="nav navbar-nav navbar-right">
                        <form class="navbar-form navbar-left" >
                             <a href="<?php echo site_url('tools_masuk/create/')?>" type="button" class="btn btn-info"><i class="glyphicon glyphicon-plus-sign"> </i>Tambah
                                    </a>
                  
                        </form>
                    </div>

                </div><!--/.nav-collapse -->
                </div>
            </div>
					
					 <!--LIST TABEL-->
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
						<h1>Daftar Tools Masuk</h1>

						 <?php if ($this->session->flashdata('pesan')): ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo  $this->session->flashdata('pesan') ?>
							</div>	
						<?php endif ?> 
						<div class="table-responsive">
						
							<table class="table table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>Kode Tools Masuk</th>
										<th>Nama Tools Masuk</th>
										<th>Tanggal Tools Masuk</th>
										<th>Jumlah Tools</th>
										<th>Distributor Tools</th>
										<th>Status</th>
										
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($tools_masuk_list as $key) { ?>
									<tr>
										<td><?php echo $key->kode_masuk ?></td>
										<td><?php echo $key->nama_tools_masuk ?></td>
										<td><?php echo $key->tanggal_masuk ?></td>
										<td><?php echo $key->jumlah ?></td>
										<td><?php echo $key->distributor ?></td>
										<td><?php echo $key->status ?></td>
										<td>
											<a href="<?php echo site_url('tools_masuk/update/').$key->kode_masuk ?>" type="button" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i>Edit
											</a>
											<a href="<?php echo site_url('tools_masuk/delete/').$key->kode_masuk ?>" type="button" class="btn btn-danger" onClick="JavaScript: return confirm('Anda yakin Hapus data ini ?')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete
											</a>
											<a href="<?php echo site_url('tools_masuk/updatestatus/').$key->kode_masuk ?>" type="button" class="btn btn-info" onClick="JavaScript: return confirm('Anda yakin Menyetujui data ini ?')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Setujui
											</a>
										</td>
									</tr>

								<?php } ?>
								
								</body>
							</table>
						</div>
					</div>
			
			


	<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="<?php echo base_url('') ?>assets/datatables.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script>
            $(document).ready(function(){
                $('#example').DataTable();
            });
        </script>
	
	</body>
</html>