<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PLTU PAITON</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/bootstrap.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container-fluid">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<nav class="navbar navbar-default" role="navigation">
							<div class="container-fluid">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<a class="navbar-brand" href="#">Title</a>
								</div>
						
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse navbar-ex1-collapse">
									<ul class="nav navbar-nav">
										<li class="active"><a href="<?php echo site_url('peminjaman') ?>">Data Peminjaman</a></li>
									</ul>
									<ul class="nav navbar-nav navbar-right">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu<b class="caret"></b></a>
											<ul class="dropdown-menu">
												<li><a href="#">Action</a></li>
												<li><a href="#">Another action</a></li>
												<li><a href="#">Something else here</a></li>
												<li><a href="#">Separated link</a></li>
											</ul>
										</li>
									</ul>
								</div><!-- /.navbar-collapse -->
						</div>
						</nav>

					</div>	

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<?php echo form_open('pengembalian_sparepart/'); ?>
								<legend>Kembalikan Buku</legend>
								<?php echo validation_errors(); ?>
								<div class="table-responsive">
								<table class="table table-hover">
									
									<label  for="">Nama Peminjam :</label>
									<?php echo $peminjaman_sparepart[0]->nama ?><br>
									<input type="hidden" name="nama" value="<?php echo $peminjaman_sparepart[0]->nama ?>">

									<label  for="">Tanggal Peminjaman :</label>
									<?php echo $peminjaman_sparepart[0]->tgl_peminjaman ?><br>
									<input type="hidden" name="tgl_peminjaman" value="<?php echo $peminjaman_sparepart[0]->tgl_peminjaman ?>">

									<label  for="">Nama Sparepart :</label>
									<?php echo $peminjaman_sparepart[0]->nama_sparepart ?><br>
									<input type="hidden" name="nama_sparepart" value="<?php echo $peminjaman_sparepart[0]->nama_sparepart ?>">

									<label  for="">Jumlah :</label>
									<?php echo $peminjaman_sparepart[0]->jumlah ?><br>
									<input type="hidden" name="jumlah" value="<?php echo $peminjaman_sparepart[0]->jumlah ?>">

									<label  for="">Status :</label>
									<?php echo $peminjaman_sparepart[0]->status ?><br>
									<input type="hidden" name="status" id="status" class="form-control" value="<?php echo $peminjaman_sparepart[0]->status ?>">

									<label for="">Tanggal Kembali (Tgl hari ini) :</label>
									<?php echo date('d-m-Y'); ?>
									
										<input type="hidden" name="tanggal_kembali" id="tanggal_kembali" class="form-control" value="<?php echo date('d-m-Y'); 
										?>"><br>
									
									 	
								<button type="submit" class="btn btn-primary">Kembalikan !</button>
								</table>
								</div>
					<?php echo form_close(); ?>
					</div>


		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>