<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PLTU PAITON</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('') ?>assets/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="<?php echo base_url('') ?>assets/DataTables/datatables.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--header-->
<div class="navbar navbar-default">
                <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">PLTU PAITON</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url('c_user');?>"><i class="glyphicon glyphicon-home"></i> USER</a></li>

                     
                    </ul>

                  

                </div><!--/.nav-collapse -->
                </div>
            </div>

<!-- sidebar-->
<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
<div class="container-fluid">
<div class="row content">
<div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" ><span class="glyphicon glyphicon-folder-close">
                                        </span> List</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil text-success"></span> <a href="<?php echo site_url('user/sparepart');?>">List Sparepart</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil"></span> <a href="<?php echo site_url('user/tools');?>">List Tools</a>
                                                </td>
                                            </tr>

                                          <!--   <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil"></span> <a href="<?php echo site_url('user/penerbit');?>">List Penerbit Buku</a>
                                                </td>
                                            </tr>
                                        
                                             
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil"></span> <a href="<?php echo site_url('user/pengarang');?>">List Pengarang Buku</a>
                                                </td>
                                            </tr> -->
                                        </table>
                                    </div>
                                </div>
                                  

                            </div>


                            <!--Transaksi-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" ><span class="glyphicon glyphicon-folder-close">
                                        </span> Transaksi</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil text-success"></span> <a href="<?php echo site_url('user/peminjaman');?>">Peminjaman</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="glyphicon glyphicon-pencil"></span> <a href="<?php echo site_url('user/laporan');?>">Laporan</a>
                                                </td>
                                            </tr>


                                            
                                        </table>
                                    </div>
                                </div>
                                  

                            </div>
                    </div>


<!--Logout -->
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="<?php echo site_url('c_user/logout');?>"><span class="glyphicon glyphicon-off">
                            </span> Logout</a>
                        </h4>
                    </div>
                    </div>   



    <!-- jQuery -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="<?php echo base_url('') ?>assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url('') ?>assets/DataTables/datatables.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script>
            $(document).ready(function(){
                $('#example').DataTable();
            });
        </script>
  </body>
</html>