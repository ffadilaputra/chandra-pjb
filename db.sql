-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: pjb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id_admin` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','admin'),(2,'admin','admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_sparepart`
--

DROP TABLE IF EXISTS `data_sparepart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_sparepart` (
  `kode_sparepart` int(50) NOT NULL AUTO_INCREMENT,
  `nama_sparepart` varchar(100) NOT NULL,
  `jumlah_sparepart` varchar(100) NOT NULL,
  `deskripsi_sparepart` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_sparepart`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_sparepart`
--

LOCK TABLES `data_sparepart` WRITE;
/*!40000 ALTER TABLE `data_sparepart` DISABLE KEYS */;
INSERT INTO `data_sparepart` VALUES (9,'obeng','','bagus'),(10,'coba','','coba');
/*!40000 ALTER TABLE `data_sparepart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_tools`
--

DROP TABLE IF EXISTS `data_tools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_tools` (
  `kode_tools` int(50) NOT NULL AUTO_INCREMENT,
  `nama_tools` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_tools`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_tools`
--

LOCK TABLES `data_tools` WRITE;
/*!40000 ALTER TABLE `data_tools` DISABLE KEYS */;
INSERT INTO `data_tools` VALUES (3,'kkll','iioo'),(4,'qww','ffff');
/*!40000 ALTER TABLE `data_tools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datauser`
--

DROP TABLE IF EXISTS `datauser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datauser` (
  `id_user` int(50) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `bagian` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datauser`
--

LOCK TABLES `datauser` WRITE;
/*!40000 ALTER TABLE `datauser` DISABLE KEYS */;
INSERT INTO `datauser` VALUES (2,'admin','admin','admin','21232f297a57a5a743894a0e4a801fc3','admin'),(8,'sinfo','sinfo','sinfo','e776f5d6f21b7cce366526752d605c44','sinfo'),(10,'supervisor','supervisor','supervisor','09348c20a019be0318387c08df7a783d','supervisor'),(11,'user','user','user','ee11cbb19052e40b07aac0ca060c23ee','user'),(12,'user','user','user','ee11cbb19052e40b07aac0ca060c23ee','user'),(13,'user','user','user','ee11cbb19052e40b07aac0ca060c23ee','user'),(14,'user','user','user','ee11cbb19052e40b07aac0ca060c23ee','user'),(15,'chandra gunawan','supervisor','chandra','202cb962ac59075b964b07152d234b70','admin');
/*!40000 ALTER TABLE `datauser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail`
--

DROP TABLE IF EXISTS `detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail` (
  `id` int(10) NOT NULL,
  `peminjaman_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sparepart_id` int(11) NOT NULL,
  `jumlah` int(50) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `status` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail`
--

LOCK TABLES `detail` WRITE;
/*!40000 ALTER TABLE `detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keranjang`
--

DROP TABLE IF EXISTS `keranjang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keranjang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sparepart_masuk_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `jatuh_tempo` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keranjang`
--

LOCK TABLES `keranjang` WRITE;
/*!40000 ALTER TABLE `keranjang` DISABLE KEYS */;
/*!40000 ALTER TABLE `keranjang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peminjaman_sparepart`
--

DROP TABLE IF EXISTS `peminjaman_sparepart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peminjaman_sparepart` (
  `kode_peminjaman` int(50) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sparepart_masuk_id` int(11) NOT NULL,
  `nama` varchar(11) NOT NULL,
  `jumlah` varchar(100) NOT NULL,
  `tanggal_peminjaman` date NOT NULL,
  `tanggal_pengembalian` date NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`kode_peminjaman`),
  KEY `sparepart_masuk_id` (`sparepart_masuk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peminjaman_sparepart`
--

LOCK TABLES `peminjaman_sparepart` WRITE;
/*!40000 ALTER TABLE `peminjaman_sparepart` DISABLE KEYS */;
INSERT INTO `peminjaman_sparepart` VALUES (1,2,24,'','5','2018-02-23','2018-06-07','kembali');
/*!40000 ALTER TABLE `peminjaman_sparepart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peminjaman_tools`
--

DROP TABLE IF EXISTS `peminjaman_tools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peminjaman_tools` (
  `kode_peminjaman` int(100) NOT NULL AUTO_INCREMENT,
  `kode_tools_dipinjam` int(100) NOT NULL,
  `id_peminjam` varchar(100) NOT NULL,
  `nama_peminjam` varchar(100) NOT NULL,
  `jumlah` varchar(100) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_peminjaman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peminjaman_tools`
--

LOCK TABLES `peminjaman_tools` WRITE;
/*!40000 ALTER TABLE `peminjaman_tools` DISABLE KEYS */;
/*!40000 ALTER TABLE `peminjaman_tools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengembalian_sparepart`
--

DROP TABLE IF EXISTS `pengembalian_sparepart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengembalian_sparepart` (
  `kode_kembali` int(50) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(50) NOT NULL,
  `id_peminjam` int(50) NOT NULL,
  `id_sparepart` int(50) NOT NULL,
  `jumlah` int(50) NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_kembali`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengembalian_sparepart`
--

LOCK TABLES `pengembalian_sparepart` WRITE;
/*!40000 ALTER TABLE `pengembalian_sparepart` DISABLE KEYS */;
/*!40000 ALTER TABLE `pengembalian_sparepart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengembalian_tools`
--

DROP TABLE IF EXISTS `pengembalian_tools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengembalian_tools` (
  `kode_kembali` int(50) NOT NULL AUTO_INCREMENT,
  `kode_tools` int(50) NOT NULL,
  `nama_tool` varchar(100) NOT NULL,
  `jenis_tools` varchar(100) NOT NULL,
  `id_peminjam` int(100) NOT NULL,
  `nama_peminjam` varchar(100) NOT NULL,
  `bagian` varchar(100) NOT NULL,
  `jumlah_pinjam` varchar(100) NOT NULL,
  `jumlah_kembali` varchar(100) NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `tanggal_peminjaman` date NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_kembali`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengembalian_tools`
--

LOCK TABLES `pengembalian_tools` WRITE;
/*!40000 ALTER TABLE `pengembalian_tools` DISABLE KEYS */;
/*!40000 ALTER TABLE `pengembalian_tools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sparepart_keluar`
--

DROP TABLE IF EXISTS `sparepart_keluar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sparepart_keluar` (
  `kode_keluar` int(50) NOT NULL AUTO_INCREMENT,
  `id_sparepart` int(50) NOT NULL,
  `nama_sparepart_keluar` varchar(100) NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `jumlah` int(50) NOT NULL,
  `distributor` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_keluar`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sparepart_keluar`
--

LOCK TABLES `sparepart_keluar` WRITE;
/*!40000 ALTER TABLE `sparepart_keluar` DISABLE KEYS */;
INSERT INTO `sparepart_keluar` VALUES (1,9,'9','2018-02-19',7,'eqwwwwwwwww1111111111111');
/*!40000 ALTER TABLE `sparepart_keluar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sparepart_masuk`
--

DROP TABLE IF EXISTS `sparepart_masuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sparepart_masuk` (
  `kode_masuk` int(50) NOT NULL AUTO_INCREMENT,
  `nama_sparepart_masuk` varchar(100) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `jumlah_masuk` int(50) NOT NULL,
  `distributor` varchar(100) NOT NULL,
  `status` enum('Disetujui','Tidak Disetujui','','') NOT NULL,
  PRIMARY KEY (`kode_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sparepart_masuk`
--

LOCK TABLES `sparepart_masuk` WRITE;
/*!40000 ALTER TABLE `sparepart_masuk` DISABLE KEYS */;
INSERT INTO `sparepart_masuk` VALUES (23,'obeng','2018-12-31',4,'eqwwwwwwwww1111111111111','Disetujui'),(24,'coba','2018-02-21',3,'eqwwwwwwwww1111111111111','Disetujui'),(25,'obeng','2018-02-27',99,'coba','Disetujui');
/*!40000 ALTER TABLE `sparepart_masuk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tools_keluar`
--

DROP TABLE IF EXISTS `tools_keluar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tools_keluar` (
  `kode_keluar` int(50) NOT NULL AUTO_INCREMENT,
  `id_tools` int(50) NOT NULL,
  `nama_tool` varchar(100) NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `jumlah` varchar(100) NOT NULL,
  `distributor` varchar(100) NOT NULL,
  PRIMARY KEY (`kode_keluar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tools_keluar`
--

LOCK TABLES `tools_keluar` WRITE;
/*!40000 ALTER TABLE `tools_keluar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tools_keluar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tools_masuk`
--

DROP TABLE IF EXISTS `tools_masuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tools_masuk` (
  `kode_masuk` int(50) NOT NULL AUTO_INCREMENT,
  `nama_tools_masuk` varchar(100) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `jumlah` varchar(100) NOT NULL,
  `distributor` varchar(100) NOT NULL,
  `status` enum('Disetujui','Tidak Disetujui','','') NOT NULL,
  PRIMARY KEY (`kode_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tools_masuk`
--

LOCK TABLES `tools_masuk` WRITE;
/*!40000 ALTER TABLE `tools_masuk` DISABLE KEYS */;
INSERT INTO `tools_masuk` VALUES (4,'','2018-02-10','5','werwer','Disetujui'),(5,'','2018-02-24','54','qwertyuiop','Disetujui'),(7,'3','2018-02-08','8','eqwej','Disetujui'),(8,'3','2018-02-19','6','coba','Disetujui');
/*!40000 ALTER TABLE `tools_masuk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `bagian` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-21  7:10:34
